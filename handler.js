'use strict';

var bwCrypto = require('./bwCrypto');
var errorCode = require('./errorCode');
var logger = require('./logger');
var config = require('./config');

const SECURITY_KEY = "sk#jDe35kl490ldk1@12";

function processProtocol(req, reply, cb) {
    if (config.development) {
        logger.log('MSG: ', JSON.stringify(req.body));
    }

    if(req.body.data === undefined || req.body.length === undefined) {
        reply.code = errorCode.INVALID_PROTOCOL;
        cb(new Error("INVALID_PROTOCOL"), null);
        return;
    }

    var msgJSONText = bwCrypto.decrypt(req.body.data, req.body.length, SECURITY_KEY);
    logger.log('msgJSONText : ' + msgJSONText);
    if(msgJSONText === '') {
        reply.code = errorCode.DECRYPT_FAIL;
        cb(new Error("DECRYPT_FAIL"), null);
        return;
    }

    cb(null, JSON.parse(msgJSONText));
}

function response(cb, reply) {
    if (config.development) {
        var stringified = JSON.stringify(reply);
        logger.log('RES: %d %s', stringified.length, stringified);
    }

    logger.log('RES Encrypt : %s', bwCrypto.encrypt(JSON.stringify(reply), SECURITY_KEY));
    cb(null, { data : bwCrypto.encrypt(JSON.stringify(reply), SECURITY_KEY) });
}

// Your first function handler
module.exports.hello = function(event, context, callback) {
    console.log('event =', event);
    console.log('remaining time =', context.getRemainingTimeInMillis());
    console.log('functionName =', context.functionName);
    console.log('AWSrequestID =', context.awsRequestId);
    console.log('logGroupName =', context.logGroupName);
    console.log('logStreamName =', context.logStreamName);
    console.log('clientContext =', context.clientContext);
    if (typeof context.identity !== 'undefined') {
        console.log('Cognito identity ID =', context.identity.cognitoIdentityId);
    }
    callback(null, { message: 'Go Serverless v1.0! Your function executed successfully!', event });
}

// You can add more handlers here, and reference them in serverless.yml
module.exports.cryptTest = function(event, context, callback) {
    var reply = {};
    processProtocol(event, reply, function(err, msgObj) {
        if(err) {
            logger.err(err);
            response(callback, reply);
        }
        else {
            if(msgObj.text === undefined) {
                reply.code = errorCode.INVALID_PROTOCOL;
                return response(callback, reply);
            }

            reply.code  = errorCode.SUCCESS;
            reply.data = true;
            response(callback, reply);
        }
    });
}