/**
 * Created by GunHoon on 2016-07-31.
 */

module.exports =  {
    SUCCESS:0,
    SERVER_IS_NOT_INITIALIZED:1,
    INVALID_PROTOCOL:2,
    NOT_FOUND_SECURITY_KEY:3,
    DECRYPT_FAIL:4,
};