/**
 * Created by dunkhoon on 2016-08-01.
 */

var util = require('util');
var config = require('./config');

module.exports.log = function(contents) {
    util.log(util.format.apply(null, arguments));
};

module.exports.err = function(contents) {
    util.log(util.format.apply(null, arguments));
};

module.exports.debug = function(contents) {
    if (config.test) {
        util.log(util.format.apply(null, arguments));
    }
};