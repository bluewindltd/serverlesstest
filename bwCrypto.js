/**
 * Created by dunkhoon on 2016-08-05.
 */

var crypto = require('crypto');
var MCrypt = require('mcrypt').MCrypt;

const IV_SIZE = 32;
const HMAC_SIZE = 32;

exports.encrypt = function(data, key) {
    var hash = crypto.createHash('sha256').update(key, 'utf8').digest();
    var iv = crypto.randomBytes(IV_SIZE);

    var mcrypt = new MCrypt('rijndael-256', 'cbc');
    mcrypt.validateKeySize(false);
    mcrypt.validateIvSize(false);
    mcrypt.open(hash, iv);

    var cryptData = mcrypt.encrypt(data);

    var outputData = new Buffer(cryptData.length + IV_SIZE + HMAC_SIZE);
    cryptData.copy(outputData, 0, 0, cryptData.length);
    iv.copy(outputData, cryptData.length, 0, IV_SIZE);

    var serverHmac = GenerateNewHMAC(hash, cryptData);
    serverHmac.copy(outputData, cryptData.length + IV_SIZE, 0, HMAC_SIZE);

    return outputData.toString('base64');
}

exports.decrypt = function(data, length, key) {
    var orgData = new Buffer(data, 'base64');
    var cryptDataLen = orgData.length - IV_SIZE - HMAC_SIZE;

    var clientHmac = new Buffer(HMAC_SIZE);
    orgData.copy(clientHmac, 0, cryptDataLen + HMAC_SIZE, orgData.length);

    var iv = new Buffer(IV_SIZE);
    orgData.copy(iv, 0, cryptDataLen, orgData.length - HMAC_SIZE);

    var cryptData = new Buffer(cryptDataLen);
    orgData.copy(cryptData, 0, 0, cryptDataLen);

    var hash = crypto.createHash('sha256').update(key, 'utf8').digest();
    var serverHmac = GenerateNewHMAC(hash, cryptData);

    if(!checkHmac(serverHmac, clientHmac)) {
        return '';
    }

    var mcrypt = new MCrypt('rijndael-256', 'cbc');
    mcrypt.validateKeySize(false);
    mcrypt.validateIvSize(false);
    mcrypt.open(hash, iv);

    return mcrypt.decrypt(cryptData).slice(0, length).toString('utf8');
    //var plaintext = mcrypt.decrypt(cryptData).slice(0, length);
    //return plaintext.toString('utf8');
}

function GenerateNewHMAC(hash, cryptData) {
    return crypto.createHmac('sha256', hash).update(cryptData).digest();
}

function checkHmac(hmac, checkHmac) {
    for(var i = 0; i < hmac.length; i++) {
        if(hmac[i] != checkHmac[i]) {
            return false;
        }
    }

    return true;
}
